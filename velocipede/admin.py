from django.contrib import admin
from .models import Velocipede, Station, Rent

# Register your models here.
admin.site.register(Velocipede)
admin.site.register(Station)
admin.site.register(Rent)