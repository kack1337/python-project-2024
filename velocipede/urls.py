from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import HomepageView, RentListView, RentedView
app_name = 'velocipede'
urlpatterns = [
  path('', HomepageView.as_view(), name="homepage"),
  path('rent', RentListView.as_view(), name="rentlist"),
  path('rented', login_required(RentedView.as_view()), name="rented"),
]
