var myCarousel = document.querySelector('#velocipedeCarousel')
var carousel = new bootstrap.Carousel(myCarousel, {
    interval: 6000,
    ride: "carousel",
    wrap: true,
    pause: "hover",
    keyboard: true,
});