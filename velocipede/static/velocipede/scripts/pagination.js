const totalPages = JSON.parse(document.querySelector("#totalPagesJSON").textContent)
const maxVisiblePages = 5;
const paginationContainer = document.querySelector('.pagination');
let currentPage = JSON.parse(document.querySelector("#currentPageJSON").textContent)
if (!(window.location.href.includes('?'))) { window.location.href += `?page=${currentPage}` }
if (!(window.location.href.includes('page='))) { window.location.href += `?page=${currentPage}` }

function generatePagination(currentPage) {
    paginationContainer.innerHTML = '';
    const paginationFragment = document.createDocumentFragment();

    const createPageItem = (page, text = page) => {
        const li = document.createElement('li');
        li.className = `page-item ${page === currentPage ? 'active' : ''}`;
        const a = document.createElement('a');
        a.className = 'page-link';
        a.href = location.href.replace(`page=${currentPage}`, `page=${page}`);
        a.textContent = text;
        a.addEventListener('click', (e) => {
            e.preventDefault();
            if (page !== currentPage) {
                currentPage = page;
                window.location.href = a.href;
                generatePagination(currentPage);
            }
        });
        li.appendChild(a);
        return li;
    };

    const createDots = () => {
        const li = document.createElement('li');
        li.className = 'page-item disabled';
        const span = document.createElement('span');
        span.className = 'page-link';
        span.textContent = '...';
        li.appendChild(span);
        return li;
    };

    // Previous button
    const prevLi = document.createElement('li');
    prevLi.className = `page-item ${currentPage === 1 ? 'disabled' : ''}`;
    const prevA = document.createElement('a');
    prevA.className = 'page-link';
    prevA.href = location.href.replace(`page=${currentPage}`, `page=${Number(currentPage)-1}`);
    prevA.setAttribute('aria-label', 'Previous');
    prevA.innerHTML = '<span aria-hidden="true">&laquo;</span>';
    prevA.addEventListener('click', (e) => {
        e.preventDefault();
        if (currentPage > 1) {
            currentPage--;
            generatePagination(currentPage);
        }
        window.location.href = prevA.href;
    });
    prevLi.appendChild(prevA);
    paginationFragment.appendChild(prevLi);

    // First few pages
    for (let i = 1; i <= Math.min(3, totalPages); i++) {
        paginationFragment.appendChild(createPageItem(i));
    }

    // Dots before the current range
    if (currentPage > 4) {
        paginationFragment.appendChild(createDots());
    }

    // Current range
    const startPage = Math.max(4, currentPage - 1);
    const endPage = Math.min(currentPage + 1, totalPages - 3);
    for (let i = startPage; i <= endPage; i++) {
        paginationFragment.appendChild(createPageItem(i));
    }

    // Dots after the current range
    if (currentPage < totalPages - 3) {
        paginationFragment.appendChild(createDots());
    }

    // Last few pages
    for (let i = Math.max(totalPages - 2, 4); i <= totalPages; i++) {
        paginationFragment.appendChild(createPageItem(i));
    }

    // Next button
    const nextLi = document.createElement('li');
    nextLi.className = `page-item ${currentPage === totalPages ? 'disabled' : ''}`;
    const nextA = document.createElement('a');
    nextA.className = 'page-link';
    nextA.href = location.href.replace(`page=${currentPage}`, `page=${Number(currentPage)+1}`);
    nextA.setAttribute('aria-label', 'Next');
    nextA.innerHTML = '<span aria-hidden="true">&raquo;</span>';
    nextA.addEventListener('click', (e) => {
        e.preventDefault();
        if (currentPage < totalPages) {
            currentPage++;
            generatePagination(currentPage);
        }
        window.location.href = nextA.href;
    });
    nextLi.appendChild(nextA);
    paginationFragment.appendChild(nextLi);

    paginationContainer.appendChild(paginationFragment);
}

generatePagination(currentPage);