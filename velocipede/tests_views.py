from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from .models import Velocipede, Rent, Station

User = get_user_model()

class HomepageViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse('velocipede:homepage')
        self.station = Station.objects.create(name='Central Station', location='123 Main St')
        Velocipede.objects.create(cost=1000, in_use=False, current_station=self.station)
        Velocipede.objects.create(cost=1200, in_use=False, current_station=self.station)
        Velocipede.objects.create(cost=1500, in_use=False, current_station=self.station)
        Velocipede.objects.create(cost=2000, in_use=False, current_station=self.station)

    def test_homepage_view(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'velocipede/homepage.html')
        self.assertIn('thumbnails', response.context)
        self.assertEqual(len(response.context['thumbnails']), 3)
        self.assertEqual(response.context['active'], 'home')

class RentedViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse('velocipede:rented')
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.client.login(username='testuser', password='12345')
        self.station = Station.objects.create(name='Central Station', location='123 Main St')
        velocipede = Velocipede.objects.create(cost=1000, in_use=True, current_station=self.station)
        Rent.objects.create(user=self.user, velocipede=velocipede, start_station=self.station)

    def test_rented_view(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'velocipede/rented.html')
        self.assertIn('rents', response.context)
        self.assertEqual(response.context['active'], 'rented')
        self.assertEqual(len(response.context['rents']), 1)
        self.assertEqual(response.context['rents'][0].user, self.user)

class RentListViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse('velocipede:rentlist')
        self.station = Station.objects.create(name='Central Station', location='123 Main St')
        Velocipede.objects.create(cost=1000, in_use=False, current_station=self.station)
        Velocipede.objects.create(cost=1200, in_use=False, current_station=self.station)

    def test_rent_list_view(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'velocipede/rentlist.html')
        self.assertIn('velocipedes', response.context)
        self.assertEqual(response.context['active'], 'to_rent')
        self.assertEqual(len(response.context['velocipedes']), 2)
