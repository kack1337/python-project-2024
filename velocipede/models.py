from django.db import models
from django.conf import settings
from django.utils import timezone

User = settings.AUTH_USER_MODEL

# Create your models here.
class Velocipede(models.Model):
  thumbnail = models.ImageField(upload_to="velocipede/images/thumbnails", height_field="thumbnail_height", width_field="thumbnail_width", max_length=None, blank=True)
  thumbnail_height = models.IntegerField(default=1280, editable=False)
  thumbnail_width = models.IntegerField(default=720, editable=False)
  cost = models.PositiveIntegerField()
  in_use = models.BooleanField()
  current_station = models.ForeignKey('Station', on_delete=models.SET_NULL, null=True, blank=True)
    
  def __str__(self):
      return f"Velocipede (ID: {self.id})"

class Station(models.Model):
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    
    def __str__(self):
        return self.name

class Rent(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    velocipede = models.ForeignKey(Velocipede, on_delete=models.CASCADE)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(null=True, blank=True)
    start_station = models.ForeignKey(Station, related_name='rent_start_station', on_delete=models.SET_NULL, null=True)
    end_station = models.ForeignKey(Station, related_name='rent_end_station', on_delete=models.SET_NULL, null=True, blank=True)
    
    def __str__(self):
        return f"Rent {self.id} by {self.user.first_name, self.user.last_name}"

class Payment(models.Model):
    rent = models.OneToOneField(Rent, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    payment_date = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return f"Payment {self.id} for Rent {self.rent_id}"