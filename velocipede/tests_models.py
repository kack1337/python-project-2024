from django.test import TestCase
from django.utils import timezone
from django.contrib.auth import get_user_model
from .models import Velocipede, Station, Rent, Payment

User = get_user_model()


class VelocipedeModelTests(TestCase):
    def setUp(self):
        self.station = Station.objects.create(name='Central Station', location='123 Main St')
        self.velocipede = Velocipede.objects.create(
            cost=1000,
            in_use=False,
            current_station=self.station
        )

    def test_velocipede_creation(self):
        self.assertIsInstance(self.velocipede, Velocipede)
        self.assertEqual(self.velocipede.thumbnail_height, 1280)
        self.assertEqual(self.velocipede.thumbnail_width, 720)
        self.assertEqual(self.velocipede.cost, 1000)
        self.assertFalse(self.velocipede.in_use)
        self.assertEqual(self.velocipede.current_station, self.station)

    def test_velocipede_str_method(self):
        self.assertEqual(str(self.velocipede), f"Velocipede (ID: {self.velocipede.id})")


class StationModelTests(TestCase):
    def setUp(self):
        self.station = Station.objects.create(name='Central Station', location='123 Main St')

    def test_station_creation(self):
        self.assertIsInstance(self.station, Station)
        self.assertEqual(self.station.name, 'Central Station')
        self.assertEqual(self.station.location, '123 Main St')

    def test_station_str_method(self):
        self.assertEqual(str(self.station), self.station.name)


class RentModelTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.station = Station.objects.create(name='Central Station', location='123 Main St')
        self.velocipede = Velocipede.objects.create(cost=1000, in_use=False, current_station=self.station)
        self.rent = Rent.objects.create(
            user=self.user,
            velocipede=self.velocipede,
            start_station=self.station
        )

    def test_rent_creation(self):
        self.assertIsInstance(self.rent, Rent)
        self.assertEqual(self.rent.user, self.user)
        self.assertEqual(self.rent.velocipede, self.velocipede)
        self.assertEqual(self.rent.start_station, self.station)
        self.assertIsNone(self.rent.end_time)
        self.assertIsNone(self.rent.end_station)

    def test_rent_str_method(self):
        self.assertEqual(str(self.rent), f"Rent {self.rent.id} by {self.user.first_name, self.user.last_name}")


class PaymentModelTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        self.station = Station.objects.create(name='Central Station', location='123 Main St')
        self.velocipede = Velocipede.objects.create(cost=1000, in_use=False, current_station=self.station)
        self.rent = Rent.objects.create(
            user=self.user,
            velocipede=self.velocipede,
            start_station=self.station
        )
        self.payment = Payment.objects.create(
            rent=self.rent,
            amount=50.00
        )

    def test_payment_creation(self):
        self.assertIsInstance(self.payment, Payment)
        self.assertEqual(self.payment.rent, self.rent)
        self.assertEqual(self.payment.amount, 50.00)

    def test_payment_str_method(self):
        self.assertEqual(str(self.payment), f"Payment {self.payment.id} for Rent {self.rent.id}")

