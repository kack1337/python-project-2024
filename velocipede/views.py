from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from django.views.generic.base import ContextMixin
from django.conf import settings
from .models import Velocipede, Rent


# Create your views here.
class HomepageView(TemplateView, ContextMixin):
  template_name = "velocipede/homepage.html"
  extra_context = {
    "thumbnails": Velocipede.objects.all()[:3],
    "active": 'home',
  }

class RentedView(TemplateView, ListView, ContextMixin):
  extra_context = {
    "active": 'rented'
  }
  template_name = "velocipede/rented.html"
  model = Rent
  object_list = Rent.objects.all()
  context_object_name = "rents"
  paginate_by = 36

  def get_queryset(self) -> QuerySet[any]:

    q = super().get_queryset()
    return q.filter(user_id=self. request.user_id)

class RentListView(TemplateView, ListView, ContextMixin):
  extra_context = {
    "active": 'to_rent'
  }
  template_name = "velocipede/rentlist.html"
  model = Velocipede
  object_list = Velocipede.objects.filter(in_use=False)
  context_object_name = "velocipedes"
  paginate_by = 36