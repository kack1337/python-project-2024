# Generated by Django 5.0.1 on 2024-05-19 20:02

import django.db.models.deletion
import django.utils.timezone
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Station',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Rent',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateTimeField(default=django.utils.timezone.now)),
                ('end_time', models.DateTimeField(blank=True, null=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('end_station', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='rent_end_station', to='velocipede.station')),
                ('start_station', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='rent_start_station', to='velocipede.station')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=10)),
                ('payment_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('rent', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='velocipede.rent')),
            ],
        ),
        migrations.CreateModel(
            name='Velocipede',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('thumbnail', models.ImageField(blank=True, height_field='thumbnail_height', upload_to='velocipede/images/thumbnails', width_field='thumbnail_width')),
                ('thumbnail_height', models.IntegerField(default=1280, editable=False)),
                ('thumbnail_width', models.IntegerField(default=720, editable=False)),
                ('cost', models.PositiveIntegerField()),
                ('in_use', models.BooleanField()),
                ('current_station', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='velocipede.station')),
            ],
        ),
        migrations.AddField(
            model_name='rent',
            name='velocipede',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='velocipede.velocipede'),
        ),
    ]
